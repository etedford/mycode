#!/usr/bin/python3

import requests

URL= "http://api.open-notify.org/astros.json"

def main():

    resp= requests.get(URL).json()

    # print(resp['people'])
    print(resp.keys())
    print()
    print()

    total_astro = len(resp.get('people'))
    print(f"People in space: {total_astro}")
    iss_crew = []
    shenzhou_crew = []

    for person in resp.get('people'):
        if person.get('craft') == "ISS":
            iss_crew.append(person.get('name'))
        else:
            shenzhou_crew.append(person.get('name'))

    print("People on the ISS: ")
    for person in iss_crew:
        print(f"{person}")
    print()
    print("People on the Shenzhou: ")
    for person in shenzhou_crew:
        print(f"{person}")

main()
