#!/usr/bin/python3

farms = [{"name": "SW Farm", "agriculture": ["chickens", "carrots", "celery"]},
         {"name": "NW Farm", "agriculture": ["sheep", "cows", "pigs", "chickens", "llamas", "cats"]},
         {"name": "East Farm", "agriculture": ["bananas", "apples", "oranges"]},
         {"name": "West Farm", "agriculture": ["pigs", "chickens", "llamas"]}]

NE_animals = farms[0]["agriculture"]

for animals in NE_animals:
    print(animals)



not_animals = ["carrots", "celery", "bananas", "apples", "oranges"]

print()

for farm in farms:
    print("-", farm["name"])
choice= input("Pick a farm!\n")

for farm in farms:
    if farm["name"].lower() == choice.lower():
        for ag_item in farm["agriculture"]:
            if ag_item not in not_animals:
                 print(ag_item)

